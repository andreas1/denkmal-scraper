import type {Config} from 'jest';

const config: Config = {
    bail: false,
    preset: 'ts-jest',
    testEnvironment: 'node',
    modulePaths: ["<rootDir>/src/"],
    setupFilesAfterEnv: ["<rootDir>/src/test/setupTestFramework.ts"]
};

export default config;