import {$load} from "./load";

describe('textVisual()', () => {
    test('removes newlines in text nodes', () => {
        let $html = $load('<span>Hello\nWorld</span>').root();
        expect($html.textVisual()).toEqual('Hello World');
    });

    test('preserves breaks', () => {
        let $html = $load('<span>Hello<br>World<br>There</span>').root();
        expect($html.textVisual()).toEqual('Hello\nWorld\nThere');
    });

    test('trims the result', () => {
        let $html = $load('<span> Hello<br></span>').root();
        expect($html.textVisual()).toEqual('Hello');
    });

    test('removes duplicate whitespace', () => {
        let $html = $load('<span>Hello   World \u00A0 There</span>').root();
        expect($html.textVisual()).toEqual('Hello World There');
    });

    test('works recursively', () => {
        let $html = $load('<span> Hello<br>World<span>This is a\ntest</span></span>').root();
        expect($html.textVisual()).toEqual('Hello\nWorldThis is a test');
    });

    test('adds newlines for p-tags', () => {
        let $html = $load('<span>Hello<p>World</p>There</span>').root();
        expect($html.textVisual()).toEqual('Hello\nWorld\nThere');
    });

    test('ignores hyperlinks', () => {
        let $html = $load('<span>Hello <a href="https://example.com">World</a> There</span>').root();
        expect($html.textVisual()).toEqual('Hello World There');
    });

    test('can handle empty HTML', () => {
        let $html = $load('').root();
        expect($html.textVisual()).toEqual('');
    });

    test('preserves multiple newlines', () => {
        let $html = $load('<span>Hello<br><br>World<br>There</span>').root();
        expect($html.textVisual()).toEqual('Hello\n\nWorld\nThere');
    });

    test('preserves tags in headings', () => {
        let $html = $load('<span><h1>Hello<br>World</h1>There</span>').root();
        expect($html.textVisual()).toEqual('Hello\nWorld\nThere');
    });

    test('does not escape markdown', () => {
        let $html = $load('<span>Hello<br>-World</span>').root();
        expect($html.textVisual()).toEqual('Hello\n-World');
    })
});
