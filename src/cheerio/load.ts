import * as cheerio from "cheerio";
import {Element} from "cheerio";

export {$load, $loadElement}

function $load(html: string): cheerio.CheerioAPI {
    let $ = cheerio.load(html);
    $.prototype.textVisual = function (): string {
        let pieces: string[] = [];
        let cursorIsInline = false;

        // https://developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements
        let blockElements = [
            'br',
            'p',
            'div',
            'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
            'li',
            'ul',
            'hr',
        ];

        function extractTextEl(el: any) {
            let isBlock = el.type === 'tag' && blockElements.includes(el.name);
            let isText = !isBlock && el.type === 'text';

            if (isBlock) {
                pieces.push('\n');
                cursorIsInline = false;
            }
            if (isText) {
                pieces.push((el.data + '').replace(/\r?\n\r?/g, ' '));
                cursorIsInline = true;
            }
            if (el.children) {
                for (let child of el.children) {
                    extractTextEl(child)
                }
            }
            if (isBlock && cursorIsInline) {
                pieces.push('\n');
                cursorIsInline = false;
            }

        }


        this.each((i : any, el:any ) => {
            extractTextEl(el);
        });

        let text = pieces.join('').trim();
        text = text.replace(/[ \u00A0]+/g, ' ');
        return text;
    };
    return $;
}

function $loadElement($e: Element): cheerio.Cheerio<Element> {
    return $load('')($e);
}

declare module 'cheerio' {
    interface Cheerio<T> {
        textVisual(this: Cheerio<T>): string;
    }
}