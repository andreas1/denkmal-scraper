export {BaseError}

type ExtraFields = { [key: string]: string | number | null };

class BaseError extends Error {
    readonly extra: ExtraFields;

    constructor(message?: string, extra?: ExtraFields) {
        super(message);
        Object.setPrototypeOf(this, new.target.prototype);
        this.extra = extra || {};
    }

    setExtra(key: string, value: string | number) {
        this.extra[key] = value;
    }

    static fromError(other: Error): BaseError {
        let error = new this(other.message);
        if (other.stack) {
            error.stack = other.stack;
        }
        return error;
    }
}
