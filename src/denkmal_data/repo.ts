import {Region, RegionVariant} from "./region";
import {Venue} from "./venue";
import {BaseError} from "../error/baseError";

export {DenkmalRepo}

class DenkmalRepo {
    regions: Region[];

    venues: Venue[];

    constructor(regions: Region[], venues: Venue[]) {
        this.regions = regions;
        this.venues = venues;
    }

    getRegionByVariant(variant: RegionVariant): Region {
        return this.getByKey(this.regions, 'variant', variant);
    }

    getRegionById(id: string): Region {
        return this.getByKey(this.regions, 'id', id);
    }

    private getByKey<T extends { [key: string]: any }>(list: T[], key: string, value: any): T {
        let item = list.find(i => i[key] === value);
        if (item === undefined) {
            throw new BaseError(`Cannot find item with '${key}=${value}'`);
        }
        return item;
    }
}
