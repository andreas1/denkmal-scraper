import {EventData} from "source/eventData";
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";

export {Source, SourcePriority}

interface Source {
    // Name of the source.
    // Should be unique per region.
    sourceName(): string;

    // Priority of the source
    sourcePriority(): SourcePriority;

    // Corresponding region of this source.
    regionVariant(): RegionVariant;

    // Fetch all events for the dates in the `request`.
    fetch(request: FetchRequest): Promise<EventData[]>;

    // Return all instances of this sources that should be fetched.
    //
    // Note: this function will be called on `.prototype` of the class.
    // It's not static because TS doesn't support static functions on interfaces.
    loadInstances(repo: DenkmalRepo): Source[];
}

enum SourcePriority {
    // A source that retrieves events from multiple venues (e.g. another event-calendar website)
    ListingWebsite = 10,

    // A source that retrieves events from the venue's facebook page
    VenueFacebookPage = 20,

    // A source that retrieves events from the venue's website directly
    VenueWebsite = 30,
}
