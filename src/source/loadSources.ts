import {Source} from "./source";
import glob from 'glob';
import path from 'path';
import {BaseError} from "../error/baseError";
import {DenkmalRepo} from "denkmal_data/repo";

export {loadSources}

function loadSources(repo: DenkmalRepo, sourceFilter?: string): Source[] {
    let files = getSourcesFiles();
    let sources: Source[] = [];

    for (let file of files) {
        let sourceExport: object = require(file);
        let sourceClass = Object.values(sourceExport).pop();
        if (!sourceClass) {
            throw new BaseError(`Source file doesn't export anything: ${file}`);
        }
        if (!('loadInstances' in sourceClass.prototype)) {
            throw new BaseError(`Source export doesn't have 'loadInstances': ${file}`);
        }
        sources.push(...sourceClass.prototype.loadInstances(repo));
    }

    if (sourceFilter) {
        sources = sources.filter(s => s.sourceName().includes(sourceFilter));
    }

    return sources;
}

function getSourcesFiles(): string[] {
    let folder = path.resolve(path.join(__dirname, '/impl/'));
    let files = glob.sync(path.join(folder, '/**/*.ts'));
    files = files.filter(f => !/\.test\.ts$/.test(f));
    return files;
}
