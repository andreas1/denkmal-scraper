import {ParseError} from "../error/parseError";

export {
    joinPhrases,
    joinDot,
    joinComma,
    joinColon,
    replaceForce,
    matchForce,
    matchOptional,
    splitForce,
    stringIncludesAnyCase
}

function joinDot(phrases: string[]): string {
    return joinPhrases(phrases, '.', true);
}

function joinComma(phrases: string[]): string {
    return joinPhrases(phrases, ',', false);
}

function joinColon(phrases: string[]): string {
    return joinPhrases(phrases, ':', false);
}

function joinPhrases(phrases: string[], punctuation: string = '.', capitalize: boolean = true): string {
    return phrases
        .map(s => s.trim())
        .filter(s => s.length > 0)
        .map(s => {
            if (capitalize) {
                s = capitalizeFirst(s);
            }
            return s;
        })
        .map((phrase, i, phrases) => {
            if (i < phrases.length - 1) {
                let endsOnPunctuation = !phrase.match(/[.!?:;,]$/u);
                if (endsOnPunctuation) {
                    phrase += punctuation;
                }
            }
            return phrase;
        })
        .join(' ');
}

function capitalizeFirst(text: string): string {
    return text.charAt(0).toLocaleUpperCase() + text.slice(1);
}

function replaceForce(text: string, search: string | RegExp, replace: string): string {
    if (null === text.match(search)) {
        throw new ParseError(`Cannot find pattern '${search}' for replacement.`, {
            'replace_search': search.toString(),
            'replace_text': text,
        });
    }
    return text.replace(search, replace);
}

function splitForce(text: string, search: string | RegExp): string[] {
    let items = text.split(search);
    if (items.length === 1) {
        throw new ParseError(`Cannot split by pattern '${search}'.`, {
            'split_search': search.toString(),
            'split_text': text,
        });
    }
    return items;
}

function matchOptional(text: string, search: RegExp): RegExpResult | undefined {
    let match = text.match(search);
    if (null === match) {
        return undefined;
    }
    let result = match;
    if (result.groups === undefined) {
        result.groups = {};
    }
    return result as RegExpResult;
}

function matchForce(text: string, search: RegExp): RegExpResult {
    let result = matchOptional(text, search);
    if (!result) {
        throw new ParseError(`Cannot find pattern '${search}'.`, {
            'match_search': search.toString(),
            'match_text': text,
        });
    }
    return result as RegExpResult;

}

function stringIncludesAnyCase(text: string, search: string): boolean {
    return text.toLocaleLowerCase().includes(search.toLocaleLowerCase());
}

// Custom version of `RegExpMatchArray` with not-optional `groups`.
interface RegExpResult {
    [key: number]: string,

    groups: {
        [key: string]: string
    }
}
