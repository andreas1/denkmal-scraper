import {Source, SourcePriority} from "source/source";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {FetchRequest} from "runner";
import {EventData, EventDataBuilder} from "source/eventData";
import {fetchUrl} from "../../../fetchUrl";
import {$load, $loadElement} from "../../../cheerio/load";

export class Kaschemme implements Source {
    sourceName(): string {
        return "kaschemme";
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Kaschemme()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let baseUrl = 'https://www.kaschemme.ch';
        let pageUrl = `${baseUrl}/programm`;
        let html = await fetchUrl(pageUrl);

        let $ = $load(html).root();
        const t = $.find('.eventlist-event--upcoming');
        let $events = $.find('.eventlist-event--upcoming').map((i, $e) => $loadElement($e)).toArray();
        return $events.map(($event) => {
            let event = new EventDataBuilder(request.now, pageUrl);
            event.venueName = 'Kaschemme';
            const $title = $event.find('.eventlist-title-link');
            event.description = $title.textVisual();
            let url: string = $title.attr('href')!;
            if (url.startsWith('/')) {
                url = `${baseUrl}${url}`
            }
            event.links.push({label: event.venueName, url: url});

            let dateRe: RegExp = /(?<year>\d{4})-(?<month>\d{1,2})-(?<day>\d{1,2})/;
            let timeRe: RegExp = /(?<hour>\d{1,2}):(?<minute>\d{2})/;
            let $eventTime = $event.find('.event-time-24hr');
            let $startTime = $eventTime.find('.event-time-24hr-start').first();
            if ($startTime.length === 1) {
                // until time on same day
                event.time.from.setByMatch($startTime.attr('datetime')!, dateRe);
                event.time.from.setByMatchOptional($startTime.text(), timeRe);

                const $endTime = $eventTime.find('.event-time-12hr-end').first();
                if ($endTime.length === 1 && $endTime.text() != $startTime.text()) {
                    event.time.until.setByMatch($endTime.attr('datetime')!, dateRe);
                    event.time.until.setByMatchOptional($endTime.text(), timeRe);
                }
            } else {
                // until time on next day
                const $fromTime = $eventTime.first();
                event.time.from.setByMatch($fromTime.attr('datetime')!, dateRe);
                event.time.from.setByMatchOptional($fromTime.text(), timeRe);

                if ($eventTime.length === 2) {
                    const $untilTime = $eventTime.last();
                    event.time.until.setByMatch($untilTime.attr('datetime')!, dateRe);
                    event.time.until.setByMatchOptional($untilTime.text(), timeRe);
                }
            }

            const bannedWords = ['eintritt', 'konzert', 'kinderdisko'];
            const $genres = $event.find('.eventlist-cats a').map((i, $e) => $loadElement($e));
            const stuff = $genres.map((i, $genre): string => {
              const s = $genre.text().toLocaleLowerCase().trim();
              return s;
            }).toArray().filter((i, tag) => {
               return true;
            });
            event.genres.add(
                $genres.map((i, $genre) => {
                    return $genre.text().toLocaleLowerCase().trim();
                }).toArray().filter((tag): boolean => {
                    for (const word of bannedWords) {
                        if (tag.includes(word)) {
                            return false;
                        }
                    }
                    return true;
                })
            );

            return event.build();
        });
    }
}