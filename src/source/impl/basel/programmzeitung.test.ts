import {Programmzeitung} from "./programmzeitung"
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('programmzeitung', () => {
    test('programmzeitung-2023-10-29', async () => {
        await nockBack('programmzeitung-2023-10-29', async () => {
            let source = new Programmzeitung();
            let events = await source.fetch(testFetchRequest('2023-10-29'));
            expect(events).toMatchSnapshot();
        });
    });
});
