import * as moment from "moment-timezone";
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {joinDot, matchForce} from "source/textHelper";
import * as de from "@mojotech/json-type-validation";
import {Decoder} from "@mojotech/json-type-validation";
import {$load} from "../../../cheerio/load";
import {Cheerio} from "cheerio";

export class Programmzeitung implements Source {

    sourceName(): string {
        return "programmzeitung";
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.ListingWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Programmzeitung()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let genres = [
            "clubbing",
            "pop",
            "blues",
            "chanson",
            "countryfolk",
            "funksoullatin",
            "gospel",
            "hiphop",
            "jazz",
            "reggae",
            "rock",
            "electronictechno",
            "volksmusikworld",
            "klassik",
        ];

        let eventsByGenre = await Promise.all(genres.map(genre => {
            return this._fetchGenre(request, genre)
        }));
        return eventsByGenre.flat();
    }


    async _fetchGenre(request: FetchRequest, genre: string): Promise<EventData[]> {
        return (await this._fetchDate(request.now, request.dateMin(), genre))
            .filter((event) => {
                return request.containsEventDate(event.time.fromAsMoment())
            })
    }

    async _fetchDate(now: moment.Moment, date: moment.Moment, genre: string): Promise<EventData[]> {
        let fromDateString = date.format('YYYY-MM-DD');
        let count = 100;
        let url = `https://proz.prog.online/Content/WebVeranstaltungen`;
        let json = await fetchUrl(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            },
            body: `xpage=celements_ajax&ajax_mode=export%2Fcollections%2Fv2&ajax=1&start=0&nb=${count}&showFields=progonEventId%2CprozEventDetailUrl%2Curls_image%2CeventGenres%2Clocation_name%2CstartDateTime%2Ctitle%2CendDateTime%2CshortText%2Cdescription%2Clocation_street%2Clocation_streetAddition%2Clocation_zip%2Clocation_city%2Clocation_url%2Curls_Booking%2CsearchInfo&maxImageHeight=466&maxImageWidth=622&fromDate=${fromDateString}&eventGenre=${genre}&sortFields=`,
        });
        let rawEvents = this._parseSchema(json);
        return rawEvents
            .map(rawEvent => this._processEvent(now, rawEvent));
    }

    _processEvent(now: moment.Moment, rawEvent: ProgrammzeitungEvent): EventData {
        let urlEventId = matchForce(rawEvent.progonEventId, /^ProgonEvent\.(.+)$/)[1];
        let urlEventTitle = encodeURI(rawEvent.title);
        let urlEvent = `https://www.proz.online/app/VeranstaltungDetail/${urlEventId}/${urlEventTitle}`;

        let event = new EventDataBuilder(now, urlEvent);
        event.venueName = rawEvent.location_name;
        event.time.from.setByMoment(moment.tz(rawEvent.startDateTime, "YYYY-MM-DDTHH:mm:ss", "Europe/Zurich"));
        if (!rawEvent.endDateTime.endsWith("00:00:00") && rawEvent.endDateTime != rawEvent.startDateTime) {
            event.time.until.setByMoment(moment.tz(rawEvent.endDateTime, "YYYY-MM-DDTHH:mm:ss", "Europe/Zurich"));
        }
        if (rawEvent.description) {
            let descriptionText = this._processDescription(rawEvent.description);
            event.description = joinDot([rawEvent.title, descriptionText]);
        } else {
            // In some rare occasions there can be no description (yet) for an event in the far future
            // In these cases we use the title only
            event.description = rawEvent.title;
        }
        return event.build();
    }

    _processDescription(descriptionHtml: string): string {
        let $text: Cheerio<any> = $load(descriptionHtml).root();
        // Remove links (usually external URLs)
        $text = $text.find('a').remove().end()

        let text = $text.text();
        text = text.replace(/\b(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}([-a-zA-Z0-9@:%_\+.~#?&//=]*)\b/, '');
        text = text.replace(/♦/, '-');
        return text;
    }

    _parseSchema(schema: any): ProgrammzeitungEvent[] {
        let decodeJson: Decoder<ProgrammzeitungSearchResult> = de.object({
            results: de.array(de.object({
                progonEventId: de.string(),
                title: de.string(),
                description: de.optional(de.string()),
                startDateTime: de.string(),
                endDateTime: de.string(),
                location_name: de.optional(de.string())
            }))
        });

        return decodeJson.runWithException(schema).results.filter((e) => e.location_name && e.location_name.length);
    }
}

type ProgrammzeitungEvent = {
    progonEventId: string,
    title: string,
    description?: string | undefined,
    startDateTime: string,
    endDateTime: string,
    location_name: string | undefined,
};

type ProgrammzeitungSearchResult = {
    results: ProgrammzeitungEvent[]
};
