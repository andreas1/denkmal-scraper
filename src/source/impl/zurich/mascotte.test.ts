import {Mascotte} from "./mascotte";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Mascotte', () => {

    test('mascotte-2020-01-22', async function () {
        await nockBack('mascotte-2020-01-22', async () => {
            let source = new Mascotte();
            let events = await source.fetch(testFetchRequest('2020-01-22'));
            expect(events).toMatchSnapshot();
        });
    });

});
