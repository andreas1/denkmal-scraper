import {Ambossrampe} from "./ambossrampe";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('ambossrampe', () => {

    test('ambossrampe-2022-11-17', async function () {
        await nockBack('ambossrampe-2022-11-17', async () => {
            let source = new Ambossrampe();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });

});
