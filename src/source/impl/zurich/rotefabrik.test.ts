import {Rotefabrik} from "./rotefabrik";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Rotefabrik', () => {

    test('rotefabrik-2019-10-11', async function () {
        await nockBack('rotefabrik-2019-10-11', async () => {
            let source = new Rotefabrik();
            let events = await source.fetch(testFetchRequest('2019-10-11'));
            expect(events).toMatchSnapshot();
        });
    });

    test('rotefabrik-2020-02-07', async function () {
        await nockBack('rotefabrik-2020-02-07', async () => {
            let source = new Rotefabrik();
            let events = await source.fetch(testFetchRequest('2020-02-07'));
            expect(events).toMatchSnapshot();
        });
    });

    test('rotefabrik-2021-07-25', async function () {
        await nockBack('rotefabrik-2021-07-25', async () => {
            let source = new Rotefabrik();
            let events = await source.fetch(testFetchRequest('2021-07-25'));
            expect(events).toMatchSnapshot();
        });
    });

});

