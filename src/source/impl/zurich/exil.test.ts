import {Exil} from "./exil";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('exil', () => {

    test('exil-2022-11-17', async function () {
        await nockBack('exil-2022-11-17', async () => {
            let source = new Exil();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });

    test('exil-2022-11-23', async function () {
        await nockBack('exil-2022-11-23', async () => {
            let source = new Exil();
            let events = await source.fetch(testFetchRequest('2022-11-23'));
            expect(events).toMatchSnapshot();
        });
    });

});
