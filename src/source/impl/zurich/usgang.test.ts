import {Usgang} from "./usgang";
import {nockBack} from "test/nockBack";
import {RegionVariant} from "denkmal_data/region";
import {testFetchRequest} from "test/testFetchRequest";


describe('Usgang', () => {

    test('usgang-2019-05-03', async function () {
        await nockBack('usgang-2019-05-03', async () => {
            let source = new Usgang(RegionVariant.zurich, '11848');
            let events = await source.fetch(testFetchRequest('2019-05-03'));
            expect(events).toMatchSnapshot();
        });
    });

    test('usgang-2019-12-24', async function () {
        await nockBack('usgang-2019-12-24', async () => {
            let source = new Usgang(RegionVariant.zurich, '11848');
            let events = await source.fetch(testFetchRequest('2019-12-24'));
            expect(events).toMatchSnapshot();
        });
    });

    test('usgang-12573-2020-01-05', async function () {
        await nockBack('usgang-12573-2020-01-05', async () => {
            let source = new Usgang(RegionVariant.zurich, '12573');
            let events = await source.fetch(testFetchRequest('2020-01-05'));
            expect(events).toMatchSnapshot();
        });
    });

    test('usgang-12573-2020-01-22', async function () {
        await nockBack('usgang-12573-2020-01-22', async () => {
            let source = new Usgang(RegionVariant.zurich, '12573');
            let events = await source.fetch(testFetchRequest('2020-01-22'));
            expect(events).toMatchSnapshot();
        });
    });

});
