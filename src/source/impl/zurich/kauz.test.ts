import {Kauz} from "./kauz";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Kauz', () => {

    test('kauz-2019-04-23', async function () {
        await nockBack('kauz-2019-04-23', async () => {
            let source = new Kauz();
            let events = await source.fetch(testFetchRequest('2019-04-23'));
            expect(events).toMatchSnapshot();
        });
    });

    test('kauz-2019-08-16', async function () {
        await nockBack('kauz-2019-08-16', async () => {
            let source = new Kauz();
            let events = await source.fetch(testFetchRequest('2019-08-16'));
            expect(events).toMatchSnapshot();
        });
    });

    test('kauz-2020-01-05', async function () {
        await nockBack('kauz-2020-01-05', async () => {
            let source = new Kauz();
            let events = await source.fetch(testFetchRequest('2020-01-05'));
            expect(events).toMatchSnapshot();
        });
    });

    test('kauz-2020-03-04', async function () {
        await nockBack('kauz-2020-03-04', async () => {
            let source = new Kauz();
            let events = await source.fetch(testFetchRequest('2020-03-04'));
            expect(events).toMatchSnapshot();
        });
    });

    test('kauz-2021-09-24', async function () {
        await nockBack('kauz-2021-09-24', async () => {
            let source = new Kauz();
            let events = await source.fetch(testFetchRequest('2021-09-24'));
            expect(events).toMatchSnapshot();
        });
    });

    test('kauz-2022-02-04', async function () {
        await nockBack('kauz-2022-02-04', async () => {
            let source = new Kauz();
            let events = await source.fetch(testFetchRequest('2022-02-04'));
            expect(events).toMatchSnapshot();
        });
    });
});
