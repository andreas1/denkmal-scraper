import {Klaus} from "./klaus";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('klaus', () => {

    test('klaus-2019-09-10', async function () {
        await nockBack('klaus-2019-09-10', async () => {
            let source = new Klaus();
            let events = await source.fetch(testFetchRequest('2019-09-10'));
            expect(events).toMatchSnapshot();
        });
    });

    test('klaus-2020-01-10', async function () {
        await nockBack('klaus-2020-01-10', async () => {
            let source = new Klaus();
            let events = await source.fetch(testFetchRequest('2020-01-10'));
            expect(events).toMatchSnapshot();
        });
    });

    test('klaus-2020-03-05', async function () {
        await nockBack('klaus-2020-03-05', async () => {
            let source = new Klaus();
            let events = await source.fetch(testFetchRequest('2020-03-05'));
            expect(events).toMatchSnapshot();
        });
    });

});
