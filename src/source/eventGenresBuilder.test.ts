import {EventGenresBuilder} from "./eventGenresBuilder";

describe('EventGenresBuilder', () => {
    let builder: EventGenresBuilder;
    beforeEach(() => {
        builder = new EventGenresBuilder();
    });

    test('add', () => {
        builder.add(['foo', 'bar']);
        expect(builder.build()).toEqual(['foo', 'bar']);
    });

    test('add with dups', () => {
        builder.add(['foo', 'foo', 'bar']);
        expect(builder.build()).toEqual(['foo', 'bar']);
    });

    test('add ignores upper/lowercase', () => {
        builder.add(['Foo', 'FOO', 'bar']);
        expect(builder.build()).toEqual(['foo', 'bar']);
    });

    test('add splits by ","', () => {
        builder.add(['Foo', 'bar,bar2']);
        expect(builder.build()).toEqual(['foo', 'bar', 'bar2']);
    });

    test('add splits by "|"', () => {
        builder.add(['Foo', 'bar|bar2']);
        expect(builder.build()).toEqual(['foo', 'bar', 'bar2']);
    });

    test('add splits by " & "', () => {
        builder.add(['Foo', 'bar & bar2 & rock&roll']);
        expect(builder.build()).toEqual(['foo', 'bar', 'bar2', 'rock&roll']);
    });

    test('add with single', () => {
        builder.add('foo');
        expect(builder.build()).toEqual(['foo']);
    });

    test('add trims', () => {
        builder.add([' foo ', '\tbar1,   bar2 ']);
        expect(builder.build()).toEqual(['foo', 'bar1', 'bar2']);
    });
});
