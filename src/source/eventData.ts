import {EventTime} from "./eventTime";
import {EventTimeBuilder} from "./eventTimeBuilder";
import {Moment} from "moment-timezone";
import {EventGenresBuilder} from "./eventGenresBuilder";

export {EventData, EventDataBuilder, EventLink}

type EventLink = { label: string, url: string };

class EventDataBuilder {
    sourceUrl: string;
    links: EventLink[] = [];
    genres: EventGenresBuilder;
    venueName?: string;
    description?: string;
    time: EventTimeBuilder;

    constructor(now: Moment, sourceUrl: string) {
        this.sourceUrl = sourceUrl;
        this.time = new EventTimeBuilder(now);
        this.genres = new EventGenresBuilder();
    }

    build(): EventData {
        if (!this.venueName) {
            throw new Error(`Missing 'venueName'`);
        }
        if (!this.description) {
            throw new Error(`Missing 'description'`);
        }
        let description = cleanText(this.description);
        let links = this.links.map(link => {
            return {label: cleanText(link.label), url: link.url};
        });
        let time = this.time.build();
        let genres = this.genres.build();
        return new EventData(this.venueName, description, time, links, genres, this.sourceUrl);
    }
}

function cleanText(text: string): string {
    text = text.trim();
    text = text.replace(/\r?\n\r?/g, ' ');
    text = text.replace(/\s+/ug, ' ');
    text = text.replace(/\[(.+?)]/g, '($1)');
    text = text.replace(/(\p{L}{2,})['`‛’‘](s? )/ugi, (_, p1, p2) => {
        return p1 + "'" + p2.toLocaleLowerCase();
    });
    text = text.replace(/(\p{L}{2,})['`‛’‘]n['`‛’‘](\p{L}{2,})/ugi, '$1\'n\'$2');
    text = text.replace(/\bDJ'?(s)?\b/gi, 'DJ$1');
    text = text.replace(/[:]$/g, '');
    text = text.replace(/([^\d])[.]$/g, '$1');
    text = text.replace(/\b::(\s)/g, ':$1');
    text = text.replace(/\b(\p{Lu})(\p{Lu}{2,})\b/gu, (_, p1, p2) => {
        return p1 + p2.toLocaleLowerCase();
    });
    text = text.charAt(0).toLocaleUpperCase() + text.slice(1);
    return text;
}

class EventData {
    sourceUrl: string;
    links: EventLink[];
    genres: string[];
    venueName: string;
    description: string;
    time: EventTime;

    constructor(venueName: string, description: string, time: EventTime, links: EventLink[], genres: string[], sourceUrl: string) {
        this.venueName = venueName;
        this.description = description;
        this.time = time;
        this.links = links;
        this.genres = genres;
        this.sourceUrl = sourceUrl;
    }
}
